//
//  manageImage.h
//  PruebaDS1
//
//  Created by Darwin Salcedo on 03/8/16.
//  Copyright © 2016 Darwin. All rights reserved.
//


#import "manageImage.h"


@implementation manageImage


+ (void) saveimage:(NSString*)url namefile:(NSString*)imageName extensionfile:(NSString*)extension{

  
    
    NSLog(@" URL %@  NAMEIMAGE %@ EXTENSION %@", url,imageName,extension);

    //Definitions
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
     NSLog(@"documentsDirectoryPath %@",documentsDirectoryPath) ;
   
    
    //Get Image From URL
    NSString * fileURL = url;
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    NSLog(@"result from download image  %@",result) ;
    if(result != NULL) {
            //Save Image to Directory
            UIImage *image = result;
            //NSString *imageName = @"MyImage";
            //NSString * extension = @"png";
            NSString * directoryPath = documentsDirectoryPath;
            if ([[extension lowercaseString] isEqualToString:@"png"]) {
                [UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"png"]] options:NSAtomicWrite error:nil];
            } else if ([[extension lowercaseString] isEqualToString:@"jpg"] || [[extension lowercaseString] isEqualToString:@"jpeg"]) {
                [UIImageJPEGRepresentation(image, 1.0) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"jpg"]] options:NSAtomicWrite error:nil];
            } else {
                NSLog(@"Image Save Failed\nExtension: (%@) is not recognized, use (PNG/JPG)", extension);
            }
            
            
            
            //get load image
            UIImage * loadImage =  result;
            NSString *fileName = imageName;
                UIImage * result2 = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.%@", directoryPath, fileName, extension]];
                
               NSLog(@"result2 %@",result2) ;
            
            //base 64
            
                NSData *imageData = UIImagePNGRepresentation(image);
                NSString * encodedString =  [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
            
            
            NSLog(@"encodedString %@",encodedString) ;
            
            
            //finish
            UIImageWriteToSavedPhotosAlbum(result, self, nil, nil);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    } else{
        NSLog(@" ERROR DOWNLOAD IMAGE  ") ;        
    }   
}
@end
