//
//  manageImage.h
//  PruebaDS1
//
//  Created by Darwin Salcedo on 03/8/16.
//  Copyright © 2016 Darwin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface manageImage : UIViewController
+ (void) saveimage:(NSString*)url namefile:(NSString*)imageName extensionfile:(NSString*)extension;

@end

